package com.af.ttoi.ttoi;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.af.ttoi.ttoi.receiver.LanguageDetailsChecker;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MyActivity";
    private TextView speakButton;
    private final int REQ_CODE_SPEECH_INPUT = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addOnClickListeners();
        Log.e(TAG, "Added on click listeners");
    }

    private void addOnClickListeners() {
        Button buttonOne = (Button) findViewById(R.id.fetch_image);
        Log.e(TAG, "Added on click listeners");
        buttonOne.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                EditText editText = (EditText) findViewById(R.id.edit_query);
                String text = editText.getText().toString();
                ImageView imageView = (ImageView) findViewById(R.id.imageView);
                imageView.setImageDrawable(null);
                Log.e(TAG, "Added on click listeners");
                makeApiCall(text, imageView);
            }
        });

        speakButton = (TextView) findViewById(R.id.btnSpeak);

        speakButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                askSpeechInput();
            }
        });
        getAllSupportedLanguages();
    }

    private void makeApiCall(final String text, final ImageView imageView) {
        Log.e(TAG, "Inside makeApiCall");
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="http://www.google.com";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        String url = "http://ec2-54-164-195-92.compute-1.amazonaws.com:8080/api-0.0.1/test/texttoimageurl?q=" + text;
                        Log.i("url", url);
                        Picasso.with(getApplicationContext()).load(url).into(imageView);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error in loading image", error.getMessage());
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    // Showing google speech input dialog

    private void askSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "Hi speak something");
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(this, "Not supported", Toast.LENGTH_SHORT);
        }
    }

    // Receiving speech input

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    Log.e("Voice input", result.get(0));
                    ImageView imageView = (ImageView) findViewById(R.id.imageView);
                    imageView.setImageDrawable(null);
                    Log.e(TAG, "Added on click listeners");
                    makeApiCall(result.get(0), imageView);
//                    voiceInput.setText(result.get(0));
                }
                break;
            }

        }
    }

    void getAllSupportedLanguages() {
        Intent detailsIntent =  new Intent(RecognizerIntent.ACTION_GET_LANGUAGE_DETAILS);
        sendOrderedBroadcast(
                detailsIntent, null, new LanguageDetailsChecker(), null, Activity.RESULT_OK, null, null);
    }
}
